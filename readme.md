### Pre-requisites:
1. Install gradle version 2.9 or Above
2. Install GIT locally
3. Install testNG eclipse plugin.
4. Install gradle eclipse plugin (buildship)
5. Java - 1.8


### Project Setup:-

 * Clone the project.
 * From Eclipse -> import -> gradle project -> select your cloned project till root directory.
 
 
### Set eclipse class path 

 * Access terminal and navigate to project location (created in eclipse)
 * Execute below commands:

```

	-> gradle clean
	-> gradle eclipse
	
```	

### Set config.properties file
* set host , port ,, protocol specific to service before running tests locally



### To add new test file [ Add as a testNg class]
* src/test/java/com/rummycircle/<feature package name>/<test file>
* add the test class reference in testng.xml


	
### Run tests from command line

* gradle test --tests org.gradle.SomeTest.someSpecificFeature
* gradle test --tests *SomeTest.someSpecificFeature
* gradle test --tests *SomeSpecificTest
* gradle test --tests *SomeSpecificTestSuite
* gradle test --tests all.in.specific.package*
* gradle test --tests *IntegTest
* gradle test --tests *IntegTest*ui*
* gradle test --tests "com.example.MyTestSuite"
* gradle test --tests "com.example.ParameterizedTest"
* gradle test --tests "*ParameterizedTest.foo*"
* gradle test --tests "*ParameterizedTest.*[2]"
* gradle someTestTask --tests *UiTest someOtherTestTask --tests *WebTest*ui


### Run tests from Eclipse
* Just run as testNG test.

	
	
	
	
	
	

