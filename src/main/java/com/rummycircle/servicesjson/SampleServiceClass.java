package com.rummycircle.servicesjson;

public class SampleServiceClass {

	private long userID;
	private long TournamentID;
	private long RPValue;
	
	public long getUserID() {
		return userID;
	}
	public void setUserID(long userID) {
		this.userID = userID;
	}
	public long getTournamentID() {
		return TournamentID;
	}
	public void setTournamentID(long tournamentID) {
		TournamentID = tournamentID;
	}
	public long getRPValue() {
		return RPValue;
	}
	public void setRPValue(long rPValue) {
		RPValue = rPValue;
	}
	
	
}
