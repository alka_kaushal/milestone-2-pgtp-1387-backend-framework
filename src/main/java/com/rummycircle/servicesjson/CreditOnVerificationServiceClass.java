package com.rummycircle.servicesjson;

public class CreditOnVerificationServiceClass {
		private long userId;
		private long txnId;
		private long rewardPointValue;

		public long getUserId() {
		return userId;
		}

		public void setUserId(long userId) {
		this.userId = userId;
		}

		public long getTxnId() {
		return txnId;
		}

		public void setTxnId(long txnId) {
		this.txnId = txnId;
		}

		public long getRewardPointValue() {
		return rewardPointValue;
		}

		public void setRewardPointValue(long rewardPointValue) {
		this.rewardPointValue = rewardPointValue;
		}

		@Override
		public String toString() {
		return "SampleServiceClass [userId=" + userId + ", txnId=" + txnId + ", rewardPointValue="
		+ rewardPointValue + "]";
		}
}
