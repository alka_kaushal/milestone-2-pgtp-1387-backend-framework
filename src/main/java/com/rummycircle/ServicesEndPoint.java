package com.rummycircle;

public interface ServicesEndPoint {
	
	// Endpoints for RPs 
	
	public static String JOIN_TOURNAMENT = "/nfs/api/rp/rct";
	public static String WITHDRAW_TOURNAMENT = "/nfs/api/rp/crpwt";
	public static String CREDIT_RP_ON_VERIFICATION ="/nfs/api/rp/crpbpl";
	public static String CREDIT_RP_POST_SETTLEMENT="/nfs/api/rp/crpps";
	
	
	// Endpoints for Tickets
	
	public static String GET_TICKET_INFO_BY_ID="/nfs/api/ticket";
	public static String GET_TICKET_LIST="/nfs/api/ticket/";
	public static String CREATE_TICKET="/nfs/api/ticket/";
	public static String UPDATE_TICKET="/nfs/api/ticket";
	public static String ASSIGN_TO_USERS="/nfs/api/ticket/assignToUser";
	

}
