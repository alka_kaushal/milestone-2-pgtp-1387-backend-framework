package com.rummycircle;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.FormEncodingType;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

public class APITestUtils {
	
	

	public static String sendGETRequestUsingWebClient(String requestURL, List<NameValuePair> queryParam) {

		WebClient webClient = new WebClient(BrowserVersion.CHROME);
		String value = "";

		webClient.getCache().clear();
		webClient.getCookieManager().clearCookies();
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		URL url;

		try {
			url = new URL(requestURL);
			WebRequest requestSettings = new WebRequest(url, HttpMethod.GET);

			if (queryParam != null) {
				requestSettings.setRequestParameters(queryParam);
				requestSettings.setEncodingType(FormEncodingType.URL_ENCODED);
			}

			Page redirectPage = webClient.getPage(requestSettings);
			Thread.sleep(5000);
			value = redirectPage.getWebResponse().getContentAsString();

		} catch (FailingHttpStatusCodeException | IOException | InterruptedException e) {
			e.printStackTrace();
		}
		webClient.close();
		return value;
	}

}
